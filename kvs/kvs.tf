resource "aws_instance" "KVS" {
  count 			 = "${var.VMCount}"
  ami = "ami-008bb5702b2a870e0"
  instance_type = "t2.micro"
  vpc_security_group_ids  = [ "${var.sgName}" ]
  /// subnet_id = "${var.subnetid}"
  associate_public_ip_address = true
  key_name = "mcd2"
  tags = {
   Name = "KVS${count.index}"
   }
}
