$json = Get-Content '.\payload.json' | Out-String | ConvertFrom-Json
#$json | ConvertTo-json
$totalVMcount = ($json.vLABCONFIG.VM).Count
$totalNICcount = ($json.vLABCONFIG.NIC).Count
if($totalNICcount -lt 1)
{
$NICname = $json.vLABCONFIG.NIC.name
$AdapterName = $json.vLABCONFIG.NIC.NetAdapterName
$SwitchType = $json.vLABCONFIG.NIC.SwitchType
}
$POSCount= 0
$WinCount= 0
$ServerCount= 0
$kioskCount= 0

$i=1
$vmnames = $json.vLABCONFIG.VM.VMName
foreach($vmname in $vmnames){
$newvmnumber = "VM"+$i
$newvmname = $vmname
$i++
}


$imagepaths = $json.vLABCONFIG.VM.VHDPath.path
foreach($imagepath in $imagepaths){
if($imagepath -like '*POS*'){$POSCount++}
elseif($imagepath -like '*KVS*'){$WinCount++}
elseif($imagepath -like '*server2012*'){$ServerCount++}
elseif($imagepath -like '*KIOSK*'){$kioskCount++}
else{write-host "not found"}
}
$ram = ($json.vLABCONFIG.VM.Ram | Get-Unique | Sort-Object -Descending | Select-Object -Last 1).ToString()
$processors = ($json.vLABCONFIG.VM.core | Get-Unique | Sort-Object -Descending | Select-Object -First 1).ToString()
$path = ($json.vLABCONFIG.VM.path) | Get-Unique
$templateVhdpath = ($json.vLABCONFIG.VM.VirtualLabVhdpath) | Get-Unique
$imagepathone= ($imagepaths | Select-Object -First 1)
$vhdxpath = Split-Path -path $imagepathone
$gscRam = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'Server2012'}).RAM

$kvsRam = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'PosReady7'}).RAM

#$kvsRam = ($json.vLABCONFIG.VM.VHDPath | Where-Object {$_.path -like '*KVS*'}).RAM
$posRam = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'Windows10 IOT'}).RAM
$kioskRam = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'KIOSK'}).RAM

# calcaulate core
$gscCore = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'Server2012'}).core

$kvsCore = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'PosReady7'}).core

$posCore = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'Windows10 IOT'}).core
$kioskCore = ($json.vLABCONFIG.VM | Where-Object {$_.Image -eq 'KIOSK'}).core

#if($json.vLABCONFIG.VM.VMName -eq "BOS_server"){

#($json.vLABCONFIG.VM | Where-Object {$_.VMName -eq 'BOS_server'}).VHDPath.path
$vmdetail = @{"VMCount"=$totalVMcount.ToString(); "posVMCount"=$POSCount.ToString(); "kvsVMCount"=$WinCount.ToString(); "gscVMCount"=$ServerCount.ToString();
"NicName"=$NICname; "NicAdapterName"=$AdapterName; "SwitchType"=$SwitchType; "ram"=$ram; "processors"=$processors;  "SwitchCount"="2"; "path"=$path; "destVhdpath"= $templateVhdpath;
"SourceVhdpath"=$vhdxpath; "min_ram"="1024"; "generation"= "1"; "gscRam" = $gscRam.ToString(); "kvsRam" = $kvsRam.ToString(); "posRam" = $posRam.ToString(); "gscCore" = $gscCore.ToString(); "kvsCore" = $kvsCore.ToString(); "posCore" = $posCore.ToString();
"kioskCore" = $kioskCore.ToString(); "kioskRam" = $kioskRam.ToString(); "kioskVMCount"=$kioskCount.ToString();
}
$vmdetail | ConvertTo-Json