resource "aws_instance" "GSC" {
  count 			 = "${var.VMCount}"
  ami = "ami-09ea8589db6ded77b"
  instance_type = "t2.micro"
  vpc_security_group_ids  = [ "${var.sgName}" ]
  /// subnet_id = "${var.subnetid}"
  associate_public_ip_address = true
  key_name = "mcd2"
  tags = {
   Name = "GSC${count.index}"
   }
}
