resource "aws_default_vpc" "default" {

}
/*
resource "aws_subnet" "vLabs_subnet" {
  vpc_id     = "${aws_vpc.vLabs_vpc.id}"
  cidr_block = "10.99.0.0/24"

  tags = {
    Name = "vLabs_subnet"
  }
}
*/
resource "aws_security_group" "vLabs_sg" {
  name        = "vLabs_sg"
  description = "Allow ports for VLabs demo"
  vpc_id      = "${aws_default_vpc.default.id}"
  
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}